import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeCatalogComponent} from "./home-catalog/home-catalog.component";

const routes: Routes = [
  {
    path: '',
    component: HomeCatalogComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }
