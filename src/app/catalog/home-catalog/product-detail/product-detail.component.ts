import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BackendMeliService} from "../../backend-meli.service";
import {Observable} from "rxjs";
import {FormatService} from "../../../shared/format/format.service";

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit{

  @Input() productId: any;
  @Output() back: EventEmitter<boolean> = new EventEmitter<boolean>();

  product$!: Observable<any>;
  description$!: Observable<any>;

  constructor(
    private backendMeliService: BackendMeliService
  ) {}


  ngOnInit(): void {
    this.product$ = this.backendMeliService.getProduct(this.productId);
    this.description$ = this.backendMeliService.getProductDescription(this.productId);
  }

  goBack() {
    this.back.emit(true);
  }

}
