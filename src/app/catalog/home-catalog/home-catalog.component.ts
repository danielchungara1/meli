import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {BackendMeliService} from "../backend-meli.service";
import {firstValueFrom, Observable} from "rxjs";
import {ColTable} from "../../shared/table/ColTable";
import {FormBuilder, FormGroup} from "@angular/forms";
import {StoreService} from "../../shared/store/store.service";
import {ErrorHandlingService} from "../../shared/error/error-handling.service";
import {SortCriteria} from "../../shared/constants/Sorting";


@Component({
  selector: 'app-home-catalog',
  templateUrl: './home-catalog.component.html',
  styleUrls: ['./home-catalog.component.css']
})
export class HomeCatalogComponent implements OnInit, AfterViewInit {

  myForm!: FormGroup;
  initialFormValues: any;
  categories$!: Observable<any>
  cols!: ColTable[];
  products!: any[];

  esDetalle: boolean = false;
  productSelected: any;
  @ViewChild('searchButton') searchButton!: ElementRef;

  constructor(
    private backendMeliService: BackendMeliService,
    private fb: FormBuilder,
    private storeService: StoreService,
    private errorHandlingService: ErrorHandlingService,
  ) {
    this.myForm = this.fb.group({
      searchText: [''],
      categoryId: [''],
      priceMax: [''],
      priceMin: [''],
      sortCriteria: ['']
    });
    this.initialFormValues = this.myForm.value;
  }

  ngOnInit(): void {
    this.cols = [{key: 'id', displayName: 'Id'}, {key: 'title', displayName: 'Descripcion'}, {
      key: 'thumbnail',
      displayName: 'Preview',
      type: 'img'
    }, {key: 'price', displayName: 'Price', type: 'coma-price'}]
    this.categories$ = this.backendMeliService.getCategories();
    this.storeService.offsetHasChanged$.subscribe(async offset => {
      try {
        await this.searchProducts(offset);
        this.storeService.updateOffset(offset);
      } catch (error: any) {
        this.errorHandlingService.handleError(error);
      }
    });
  }

  ngAfterViewInit(): void {
    // TODO: Remove it, only for testing.
    this.searchText = 'apple';
    this.searchButton.nativeElement.click();
  }

  async searchProducts(offset: number) {
    const response: any = await firstValueFrom(
      this.backendMeliService.searchProducts(
        this.categoryId,
        offset,
        this.sortCriteria,
        this.priceMin,
        this.priceMax,
        this.searchText
      ));
    this.products = response.results;
    this.storeService.updateTotalItems(response.paging.total);
  }

  async resetFilters() {
    this.myForm.setValue(this.initialFormValues);
    await this.searchProducts(0);
  }

  sortToLowerPrice() {
    this.sortCriteria = SortCriteria.LOWER_PRICE;
    this.storeService.offsetChanged(0);
  }

  sortToGraterPrice() {
    this.sortCriteria = SortCriteria.HIGHER_PRICE;
    this.storeService.offsetChanged(0);
  }

  sortByRelevance() {
    this.sortCriteria = SortCriteria.RELEVANCE;
    this.storeService.offsetChanged(0);
  }

  onProductSelected($event: any) {
    if($event) {
      this.productSelected = $event;
      this.esDetalle = true;
    }
  }

  onBack($event: boolean) {
    if($event) {
      this.esDetalle = false;
    }
  }

  get categoryId(): string {
    return this.myForm.get('categoryId')!.value;
  }

  set categoryId(value: string) {
    this.myForm.get('categoryId')!.setValue(value);
  }

  get searchText(): string {
    return this.myForm.get('searchText')!.value;
  }

  set searchText(value: string) {
    this.myForm.get('searchText')!.setValue(value);
  }

  get sortCriteria(): string {
    return this.myForm.get('sortCriteria')!.value;
  }

  set sortCriteria(value: string) {
    this.myForm.get('sortCriteria')!.setValue(value);
  }

  get priceMin(): number {
    return this.myForm.get('priceMin')!.value;
  }

  set priceMin(value: number) {
    this.myForm.get('priceMin')!.setValue(value);
  }

  get priceMax(): number {
    return this.myForm.get('priceMax')!.value;
  }

  set priceMax(value: number) {
    this.myForm.get('priceMax')!.setValue(value);
  }

}
