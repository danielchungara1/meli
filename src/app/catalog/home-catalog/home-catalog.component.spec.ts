import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCatalogComponent } from './home-catalog.component';

describe('HomeComponent', () => {
  let component: HomeCatalogComponent;
  let fixture: ComponentFixture<HomeCatalogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HomeCatalogComponent]
    });
    fixture = TestBed.createComponent(HomeCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
