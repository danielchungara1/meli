import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from "@angular/common/http";
import {shareReplay} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class BackendMeliService {

  private readonly API_URL = environment.apiUrl;
  private readonly ENDPOINT_CATEGORIES = this.API_URL + '/sites/MLA/categories';
  private readonly ENDPOINT_PRODUCTS = this.API_URL + '/sites/MLA/search';
  private readonly ENDPOINT_PRODUCT_DETAILS = this.API_URL + '/items';
  private readonly HIDE_SPINNER = {hideSpinner: 'true'}
  private readonly MAX_PRICE_VALUE = 999999999;

  constructor(
    private http: HttpClient,
  ) {
  }

  getCategories() {
    return this.http.get(
      this.ENDPOINT_CATEGORIES, {params: this.HIDE_SPINNER}
    ).pipe(
      shareReplay(),
    );
  }

  searchProducts(
    categoryId: string,
    offset: number,
    sortCriteria: string,
    priceMin: number,
    priceMax: number,
    searchText: string,
  ) {
    const queryParams: any = {
      category: categoryId,
      q: searchText,
      offset,
      sort: sortCriteria,
      price: this.getPriceRange(priceMin, priceMax)
    };
    return this.http.get(this.ENDPOINT_PRODUCTS, {params: queryParams}).pipe(
      shareReplay(),
    );
  }

  getProduct(productId: string) {
    return this.http.get(
      `${this.ENDPOINT_PRODUCT_DETAILS}/${productId}`, {params: {}}
    ).pipe(
      shareReplay(),
    );
  }

  getProductDescription(productId: string) {
    return this.http.get(
      `${this.ENDPOINT_PRODUCT_DETAILS}/${productId}/description`, {params: {}}
    ).pipe(
      shareReplay(),
    );
  }

  private getPriceRange(priceMin: number, priceMax: number) {
    if (!priceMin && !priceMax) {
      return '';
    }
    priceMin = !priceMin ? 0 : priceMin;
    priceMax = !priceMax ? this.MAX_PRICE_VALUE : priceMax;
    return `${priceMin}-${priceMax}`
  }

}
