import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CatalogRoutingModule} from './catalog-routing.module';
import {HomeCatalogComponent} from './home-catalog/home-catalog.component';
import {SharedModule} from "../shared/shared.module";
import {ReactiveFormsModule} from "@angular/forms";
import { ProductDetailComponent } from './home-catalog/product-detail/product-detail.component';


@NgModule({
  declarations: [
    HomeCatalogComponent,
    ProductDetailComponent,
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class CatalogModule {
}
