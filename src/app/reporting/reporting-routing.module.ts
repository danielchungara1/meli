import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeReportComponent} from "./home-report/home-report.component";
import {ReportComponent} from "./report/report.component";

const routes: Routes = [
  {
    path: '',
    component: HomeReportComponent,
    children: [
      {
        path: 'reports',
        component: ReportComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportingRoutingModule {
}
