import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportingRoutingModule } from './reporting-routing.module';
import { ReportComponent } from './report/report.component';
import { HomeReportComponent } from './home-report/home-report.component';


@NgModule({
  declarations: [
    ReportComponent,
    HomeReportComponent
  ],
  imports: [
    CommonModule,
    ReportingRoutingModule
  ]
})
export class ReportingModule { }
