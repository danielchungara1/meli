export const SortCriteria = {
  RELEVANCE: 'relevance',
  LOWER_PRICE: 'price_asc',
  HIGHER_PRICE: 'price_desc'
}
