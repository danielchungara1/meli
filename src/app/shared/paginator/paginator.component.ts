import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {StoreService} from "../store/store.service";

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent {

  @Input() show = false;
  private readonly PAGE_SIZE = 50;
  protected totalItems!: number;
  private offset: number = 0;

  constructor(
    private storeService: StoreService
  ) {
    this.storeService.updateTotalItems$.subscribe(totalItems => {
      if(totalItems) {
        this.totalItems = totalItems;
      }
    })
    this.storeService.updateOffset$.subscribe(value => {
        this.offset = value;
        this.moveViewToTop();
    })
  }

  async getFirstPage() {
    this.storeService.offsetChanged(0);
  }

  async getNextPage() {
    this.storeService.offsetChanged(this.offset + this.PAGE_SIZE);
  }

  async getPreviousPage() {
    this.storeService.offsetChanged(this.offset - this.PAGE_SIZE);
  }

  async getLastpage() {
    this.storeService.offsetChanged(Math.floor(this.totalItems / 50) * 50);
  }

  isLastOne() {
    return (this.offset + 50) > this.totalItems;
  }

  isFirstOne() {
    return this.offset == 0;
  }

  moveViewToTop() {
    const scrollY = window.scrollY;
    const umbral = 200; // Min value that the user has scrolled down [px]
    if (scrollY > umbral) {
      window.scrollTo(0, 0);
    }

  }
}
