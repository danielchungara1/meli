import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  private readonly PRODUCT_ID = 'productId';

  constructor() { }

  setLastProductId(value: string) {
    this.setItem(this.PRODUCT_ID, value);
  }
  getLastProductId(): string {
    return this.getItem(this.PRODUCT_ID);
  }
  removeLastProductId(){
    this.removeItem(this.PRODUCT_ID);
  }

  private setItem(key: string, value: string): void {
    localStorage.setItem(key, value);
  }
  private getItem(key: string): string {
    return localStorage.getItem(key) || "" ;
  }
  private removeItem(key: string): void {
    localStorage.removeItem(key);
  }

}
