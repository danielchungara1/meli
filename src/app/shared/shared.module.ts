import { NgModule } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import { TableComponent } from './table/table.component';
import { SpinnerComponent } from './spinner/spinner.component';
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {SpinnerInterceptor} from "./spinner/spinner.interceptor";
import { PaginatorComponent } from './paginator/paginator.component';
import { PriceFormatPipe } from './format/price-format.pipe';



@NgModule({
  declarations: [
    TableComponent,
    SpinnerComponent,
    PaginatorComponent,
    PriceFormatPipe
  ],
    imports: [
        CommonModule,
        NgOptimizedImage
    ],
  exports: [
    TableComponent,
    SpinnerComponent,
    PaginatorComponent,
    PriceFormatPipe
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: SpinnerInterceptor, multi: true },
  ]
})
export class SharedModule { }
