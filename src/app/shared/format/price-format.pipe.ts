import { Pipe, PipeTransform } from '@angular/core';
import {FormatService} from "./format.service";

@Pipe({
  name: 'priceFormat'
})
export class PriceFormatPipe implements PipeTransform {

  constructor(
    private formatService: FormatService
  ) {
  }
  transform(value: number, ...args: unknown[]): string {
    return this.formatService.formatPrice(value);
  }

}
