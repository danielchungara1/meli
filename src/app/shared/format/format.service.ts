import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormatService {

  constructor() { }

  formatPrice(price: number): string {
    if(!price) { return '' }
    try {
      let result = price.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' });
      result = result.replace('€', '');
      if (result.includes(',')) {
        const index = result.indexOf(',')
        if(index !== -1 && Number(result.substring(index+1, result.length-1)) === 0) {
          result = result.substring(0, index);
        }
      }
      return `$${result}`;
    } catch (error) {
      console.error(error);
      return String(price);
    }
  }
}
