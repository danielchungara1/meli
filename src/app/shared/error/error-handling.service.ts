import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlingService {

  constructor() { }

  handleError(error: any) {
    if (error.error && error.error.message) {
      alert(error.error.message)
    }
    console.error(error);
  }

}
