import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  private showSpinnerSub: Subject<boolean> = new Subject<boolean>();
  showSpinner$: Observable<boolean> = this.showSpinnerSub.asObservable();

  constructor() { }

  spinnerOn() {
    this.showSpinnerSub.next(true);
  }
  spinnerOff() {
    this.showSpinnerSub.next(false);
  }

}
