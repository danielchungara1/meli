import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {finalize, Observable} from 'rxjs';
import {SpinnerService} from "./spinner.service";

@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {

  constructor(private spinnerService: SpinnerService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!request.params.has('hideSpinner')) {
      this.spinnerService.spinnerOn();
    } else {
      if (request.params.get('hideSpinner') !== 'true') {
        this.spinnerService.spinnerOn();
      }
      request = request.clone({
        params: request.params.delete('hideSpinner'),
      });
    }

    return next.handle(request).pipe(
      finalize(() => {
        this.spinnerService.spinnerOff();
      })
    );

  }
}

/*
if (request.params.has('showSpinner')) {
  if (request.params.get('showSpinner') == 'true') {
    this.spinnerService.spinnerOn();
  }
  request = request.clone({
    params: request.params.delete('showSpinner'),
  });
}
*/
