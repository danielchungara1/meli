import {Component, Input} from '@angular/core';
import {SpinnerService} from "./spinner.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent {

  showSpinner$!: Observable<boolean>;

  constructor(
    private spinnerService: SpinnerService
  ) {
    this.showSpinner$ = this.spinnerService.showSpinner$;
  }


}
