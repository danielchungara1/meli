import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private offsetHasChangedSub: BehaviorSubject<number> = new BehaviorSubject(0);
  offsetHasChanged$: Observable<number> = this.offsetHasChangedSub.asObservable();

  private updateOffsetSub: BehaviorSubject<number> = new BehaviorSubject(0);
  updateOffset$: Observable<number> = this.updateOffsetSub.asObservable();
  private updateTotalItemsSub: BehaviorSubject<number> = new BehaviorSubject(0);
  updateTotalItems$: Observable<number> = this.updateTotalItemsSub.asObservable();

  constructor() {
  }

  offsetChanged(offset: number) {
    this.offsetHasChangedSub.next(offset);
  }

  updateTotalItems(total: number) {
    this.updateTotalItemsSub.next(total);
  }

  updateOffset(value: number) {
    this.updateOffsetSub.next(value);
  }

}
