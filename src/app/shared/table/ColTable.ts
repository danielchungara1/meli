export interface ColTable {
  key: string;
  displayName: string;
  type?: string;
}
