import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ColTable} from "./ColTable";
import {formatCurrency} from "@angular/common";
import {FormatService} from "../format/format.service";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent {

  @Input() cols: ColTable[] = [];
  @Input() data: any[] = [];
  @Input() title: string = '';
  @Output() itemSelected: EventEmitter<any> = new EventEmitter<any>();

  protected readonly formatCurrency = formatCurrency;

  constructor(
    private formatService: FormatService
  ) {}


  formatPrice(price: number) {
    return this.formatService.formatPrice(price);
  }

  onClickItem(item: any) {
    this.itemSelected.emit(item)
  }

}
