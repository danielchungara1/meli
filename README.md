# MELI
Frontend

## Tareas Pendientes


## Backlog
- [ ] Agregar en la url los query params 

## Icons
https://icons.getbootstrap.com

## Tareas Finalizadas
- [x] Corregir paginador cuando se avanza y se vuelve atras.
- [x] Agregar nueva pagina para ver los precio del producto/publicacion y corregir estilos.
- [x] Agregar un spinner y evaluar colocar un interceptor para cada llamada o ser selectivo.
- [x] Agregar Paginador de productos <| < current > |>
- [x] Agregar handler de errores del backend y mostrar alert
- [x] Agregar una imagen a cada resultado
- [x] Agregar funcion para resetear el formulario de busqueda
- [x] Agregar manejo de errores solido, con mayor flexibilidad y mantenibilidad
- [x] Agregar columna precio y con formato $99.999,99
- [x] Bug al navegar a la ultima pagina y con resultado erroneo el cartel de error aparece 2 veces, quitar el 2do.
- [x] Agregar filtros para ordenar por precio (mayor y menor), relevancia.
- [x] Agregar filtro de rango de precios. (Corretir el caso donde se envia '-', sin o con 1 limite)
- [x] Agregar nueva pagina para ver los detalles del producto/publicacion, imagenes en HD, descripcion de la publicacion.
